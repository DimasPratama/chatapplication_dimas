import React,{Component} from 'react';
import {
    Platform, StyleSheet, Text,
    AsyncStorage, TouchableOpacity,
    TextInput,View,KeyboardAvoidingView
} from "react-native";
import User from '../User'

import firebase from 'firebase'


export default class LoginScreen extends Component{
    static  navigationOptions ={
        header :null
    }

    state = {
        phone:'',
        name:''
    }

    componentWillMount(){
        AsyncStorage.getItem('userPhone').then(yal =>{
            if(yal){
               this.setState({phone:val})
            }

        } )

    }
    handleChange = key => val =>{
        this.setState({[key]:val})
    }

    submitFrom = async ()=> {
        if (this.state.name =='' | this.state.phone == ''){
            alert(this.state.phone + '\n'+this.state.name)
        }else{
            await AsyncStorage.setItem('userPhone',this.state.phone)
            User.phone = this.state.phone
            firebase.database().ref('users/'+User.phone).set({name:this.state.name})

            this.props.navigation.navigate('App')
            alert('phone successfully saved')
        }
    }
    render(){
        return(
            <View style={styles.container}>

                <View style={{alignItems: 'center',marginBottom:80}}>
                    <Text style={{fontSize:20,fontWeight: 300,marginBottom: 15}}> Welcome to KakaoTalk</Text>
                    <Text style={{color:'#95A5A6'}}> if you have a Kakao Account,</Text>
                    <Text style={{color:'#95A5A6'}}>log in with your email or phone number </Text>

                </View>


                    <TextInput
                        placeholder = "Email or phone number"
                        style = {styles.input}
                        value={this.state.name}
                        returnKeyType="next"
                        onSubmitEditing={()=>this.passwordInput.focus()}
                        onChangeText = {this.handleChange('name')}
                    />
                    <TextInput
                        secureTextEntry
                        placeholder="Password"
                        ref={(input)=>this.passwordInput = input}
                        style = {styles.input}
                        value={this.state.phone}
                        onChangeText = {this.handleChange('phone')}
                    />


                <TouchableOpacity
                    style={{backgroundColor:'#F2F4F4',width:'90%',height: '7%',alignItems:'center',justifyContent:'center',borderRadius: 5,marginTop: 10}}
                    onPress={this.submitFrom}>
                    <Text>Log In</Text>
                </TouchableOpacity>


                    <TouchableOpacity
                        style={{backgroundColor:'#F2F4F4',width:'90%',height: '7%',alignItems:'center',justifyContent:'center',borderRadius: 5,marginTop: 10}}
                        onPress={this.submitFrom}>
                        <Text>Sign Up</Text>
                    </TouchableOpacity>

                <TouchableOpacity
                    style={{height: '7%',alignItems:'center',justifyContent:'center'}}
                    onPress={this.submitFrom}>
                    <Text style={{fontSize:12}}>Find Kakao Account or Password</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
   container:{
       paddingTop: Platform.OS === 'ios' ? '35%' : '28%',
       flex:1,
       alignItems:'center',
       backgroundColor:'#F5FCFF'

   },

   input:{
       height: 30,
       paddingHorizontal: 10,
       fontSize:16,
       borderBottomWidth:1,
       borderColor:'#ccc',
       width:'90%',
       marginBottom:10,
       borderRadius:5,

   }
});
