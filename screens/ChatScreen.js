import React from 'react';
import {
    SafeAreaView,
    Keyboard,
    Text,
    View,
    TextInput,
    Dimensions,
    FlatList,
    TouchableOpacity,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
    ScrollView,
    Image
} from "react-native";
import styles from '../constants/styles'
import User from '../User'
import firebase from 'firebase'


export default class ChatScreen  extends React.Component{
    static navigationOptions = ({navigation})=> {
        return{
            title:navigation.getParam('name',null),
            headerStyle:{backgroundColor:'#abc3d3',borderBottomWidth: 0,},
            headerRight: (

                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity>
                        <Image source={require('../images/common-search-lookup-glyph-512.png')} style={{width:20,height:20,marginRight:10}}/>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>navigation.navigate('Profile')}>
                        <Image source={require('../images/002-list-1.png')} style={{width:20,height:20,marginRight:10}}/>
                    </TouchableOpacity>
                </View>

            )
        }
    }
    constructor(props){
        super(props)
        this.state ={
            person:{
                name : props.navigation.getParam('name'),
                phone : props.navigation.getParam('phone')
            },
            textMessage:'',
            messageList:[]
        }
    }

    componentWillMount(){
        console.log('haii')
        firebase.database().ref('messages').child(User.phone).child(this.state.person.phone)
            .on('child_added',(value)=> {
                this.setState ((prevState) =>{
                    return{
                        messageList:[...prevState.messageList, value.val()]
                    }
                })
            })
    }
    handleChange = key => val => {
        this.setState({[key]:val})
    }

    convertTime = (time) => {
        let d = new Date(time)
        let c = new Date();
        let result = (d.getHours()<10?'0':'')+d.getHours()+':';
        result += (d.getMinutes()<10?'0':''+d.getMinutes());
        if(c.getDay() !== d.getDay()){
            result = ' '+result
        }
        return result
    }

    sendMessage = async () => {
        if(this.state.textMessage.length > 0){
            let msgId = firebase.database().ref('message').child(User.phone).child(this.state.person.phone).push().key
            let updates = {}
            let message = {
                message : this.state.textMessage,
                time: firebase.database.ServerValue.TIMESTAMP,
                from: User.phone
            }
            updates['messages/'+User.phone+'/'+this.state.person.phone+'/'+msgId] = message;
            updates['messages/'+this.state.person.phone+'/'+User.phone+'/'+msgId] = message;
            firebase.database().ref().update(updates)
            this.setState({textMessage:''})
        }
    }

    renderRow = ({item}) => {
        return(
            <View style ={{
                flexDirection:'row',
                width:'60%',
                alignSelf:item.from === User.phone ? 'flex-end' : 'flex-start',
                backgroundColor:item.from === User.phone ? 'white':'white',
                borderRadius:5,
                marginBottom:10
            }}>
                <Text style={{color:'black',padding:7, fontSize:16}}>{item.message}</Text>
                <Text style={{color:'black',padding:3, fontSize:12}}> {this.convertTime(item.time)}</Text>

            </View>
        )

    }

    render(){
        let {height, width} = Dimensions.get('window')
        return(
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1 }}
                keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
            >
                <SafeAreaView style={{flex:1,backgroundColor: 'white'}}>

                        <View style={{marginBottom:20, flex: 1, justifyContent: "flex-end",backgroundColor:'#abc3d3'}}>

                            <FlatList
                                style={{padding: 10, height: height*0.8}}
                                data={this.state.messageList}
                                renderItem={this.renderRow}
                                keyExtractor={(item,index) => index.toString()}
                            />

                            <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 5,paddingTop:5,width:'100%',backgroundColor:'white'}}>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.textMessage}
                                    placeholder={"Type Message ..."}
                                    onChangeText={this.handleChange('textMessage')}
                                />
                                <TouchableOpacity onPress={this.sendMessage} style={{paddingBottom: 10, marginLeft: 10,}}>
                                    <Image source={require('../images/send-button.png')} style={{width:25,height:20}}/>
                                </TouchableOpacity>
                            </View>

                             </View>
                    
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }

}