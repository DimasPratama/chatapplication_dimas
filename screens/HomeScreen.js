import React from 'react'
import {View,SafeAreaView, Image, Text, TouchableOpacity, AsyncStorage, FlatList} from "react-native";
import User from'../User'
import styles from '../constants/styles'
import firebase from 'firebase'

export default class HomeScreen extends React.Component{
    static navigationOptions = ({navigation})=>{
        return{
            title: 'Chats',
            headerStyle:{backgroundColor:'white',borderBottomWidth: 0,},
            headerRight: (

                <View style={{flexDirection:'row'}}>
                <TouchableOpacity onPress={()=>navigation.navigate('Profile')}>
                    <Image source={require('../images/common-search-lookup-glyph-512.png')} style={{width:20,height:20,marginRight:10}}/>
                </TouchableOpacity>

                    <TouchableOpacity onPress={()=>navigation.navigate('Profile')}>
                        <Image source={require('../images/Windows_Settings_app_icon.png')} style={{width:20,height:20,marginRight:10}}/>
                    </TouchableOpacity>

                </View>
            )
        }
    }
    state = {
        users:[]
    }

    componentWillMount(){
        let dbRef = firebase.database().ref('users')
        dbRef.on('child_added',(val)=>{
            let person = val.val();
            person.phone = val.key;
            if(person.phone === User.phone){
                User.name = person.name
            }else{
                this.setState((prevState)=>{
                    return{
                        users: [...prevState.users, person]
                    }
                })
            }
        })
    }
    renderRow = ({item}) =>{
        return(
            <TouchableOpacity
                onPress={()=> this.props.navigation.navigate('Chat',item)}
                style={{padding:10,flexDirection:'row',alignItems:'center'}}>
                <View style={{borderRadius:15,width:40,height:40,backgroundColor:'black',alignItems:'center',justifyContent:'center',marginRight: 10}}>
                    <Text style={{fontSize:20,color:'white'}}>{item.name[0]}</Text>
                </View>
                <Text style={{fontSize:20}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    render(){
        return(
            <SafeAreaView style = {styles.container}>
            <FlatList
                style={{width:'100%',backgroundColor:'white'}}
                data = {this.state.users}
                renderItem={this.renderRow}
                keyExtractor={(item)=> item.phone}
            />
            </SafeAreaView>
        )
    }
}

