import {StyleSheet} from "react-native";


const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
    input:{
        padding:10,
        borderWidth:1,
        borderColor:'#ccc',
        width:'85%',
        marginBottom:10,
        borderRadius:15
    },
    btnText:{
        fontSize:20,
        color:'#CC4F41'
    }

});

export default styles